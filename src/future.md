# Future enhancements

Some of this is already in the works; some of it may never happen.

* Move certificate/key to a [TPM](oc_tpm).
* Really, there are 3 levels of access here:
  * system modification (ip addresses, routes, etc) - required to be root
  * handling of (potentially dangerous) VPN traffic - openconnect user
  * client authentication - currently openconnect, but should maybe be
    `waldrep` or something similar.
    See the `--authenticate` option in the [man page](man).
* Further `systemd` isolation

[oc_tpm]: https://www.infradead.org/openconnect/tpm.html
[man]: https://www.infradead.org/openconnect/manual.html
