# openconnect VPN on a router

This is a bunch of notes on how to setup an openconnect vpn on a Linux box
running as a router.
I'm running Archlinux on a box called `lithium`.

## Basic setup
* Install openconnect>=8.09
* Setup an openconnect user.
  This user will run the `openconnect` binary.
* Edit sudoers to allow openconnect to run `vpnc-script`.
  This script is used by openconnect to setup addresses, routes, etc.
* Use systemd-networkd to create a tun device for the openconnect user.
* Create a systemd service to run openconnect

---

[Source][src] for this document.
Large and small contributions welcome!


[src]: https://gitlab.com/a172/lithium-openconnect
