# openconnect.service

Create `/etc/systemd/system`.

```
[Unit]
Description = openconnect vpn
Documentation = man:openconnect(8)
After = network-online.target

[Install]
WantedBy = multi-user.target

[Service]
ExecReload =+ /bin/kill -SIGUSR2 $MAINPID
ExecStart = /usr/bin/openconnect --config=/etc/openconnect/nis.config 'https://vpn.nis.vt.edu/nis/certauth'
Type = exec
User = openconnect
Group = openconnect
PrivateTmp = true
ProtectHome = true
ProtectControlGroups = true
ProtectSystem = strict
```
