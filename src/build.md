# Build openconnect

As of 2020-05-07, the latest version of openconnect in the Arch Linux
repositories is 8.05.
This version has issues with certificate authentication with pulse.
This is a PKGBUILD for 8.09, which is working.

```
# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Felix Yan <felixonmars@archlinux.org>

pkgname=openconnect
pkgver=8.09
pkgrel=1
epoch=1
pkgdesc='Open client for Cisco AnyConnect VPN'
url='https://www.infradead.org/openconnect/'
arch=('x86_64')
license=('LGPL2.1')
depends=('gnutls' 'krb5' 'libproxy' 'libstoken.so' 'libxml2' 'lz4'
         'oath-toolkit' 'pcsclite' 'stoken' 'tpm2-tss' 'vpnc')
makedepends=('intltool' 'python2')
optdepends=('python2: tncc-wrapper')
provides=('libopenconnect.so')
options=('!emptydirs')
source=(ftp://ftp.infradead.org/pub/openconnect/openconnect-${pkgver}.tar.gz{,.asc})
sha256sums=('f39802be4c3a099b211ee4cc3318b3a9a195075deab0b4c1c5880c69340ce9a6'
            'SKIP')
validpgpkeys=('BE07D9FD54809AB2C4B0FF5F63762CDA67E2F359') # David Woodhouse <dwmw2@infradead.org>

build() {
  cd ${pkgname}-${pkgver}
  PYTHON=/usr/bin/python2 \
    ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib \
    --disable-static
  make
  make check
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
}

# vim: ts=2 sw=2 et:
```
