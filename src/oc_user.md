# Create an unprivileged user

Having something run as root connect to something on the Internet for
instructions and run an Internet facing service is a bad idea.

## Create the user

```bash
useradd \
  --home-dir /etc/openconnect \
  --no-create-home \
  --user-group \
  --shell /usr/bin/nologin \
  --comment openconnect \
  openconnect
```

## Create the homedir

We don't create it with the user because we don't want to copy over `/etc/skel`.

```bash
mkdir /etc/openconnect
chmod 755 /etc/openconnect
chown openconnect.openconnect /etc/openconnect
```
