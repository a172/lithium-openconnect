# sudo

`vpnc-script` is what sets up the addresses, routes, etc.
This requires root privileges.

`vpnc`, running as openconnect, runs `vpnc-script`, as root via sudo.
Parameters are passed in a dynamic set of environment variables.
`sudo` by default resets the execution environment.

The exact variables set is dynamic, but we can catch them all with some simple
matching.
Double check the comments in `/etc/vpnc/vpnc-script` for an updated list.

Create `/etc/sudoers.d/openconnect` via `visudo -f openconnect`:

```sudoers
Defaults env_keep += reason
Defaults env_keep += VPNGATEWAY
Defaults env_keep += TUNDEV
Defaults env_keep += INTERNAL_IP4_ADDRESS
Defaults env_keep += INTERNAL_IP4_MTU
Defaults env_keep += INTERNAL_IP4_NETMASK
Defaults env_keep += INTERNAL_IP4_NETMASKLEN
Defaults env_keep += INTERNAL_IP4_NETADDR
Defaults env_keep += INTERNAL_IP4_DNS
Defaults env_keep += INTERNAL_IP4_NBNS
Defaults env_keep += INTERNAL_IP6_ADDRESS
Defaults env_keep += INTERNAL_IP6_NETMASK
Defaults env_keep += INTERNAL_IP6_DNS
Defaults env_keep += CISCO_DEF_DOMAIN
Defaults env_keep += CISCO_BANNER
Defaults env_keep += CISCO_SPLIT_INC
Defaults env_keep += CISCO_SPLIT_INC_*_ADDR
Defaults env_keep += CISCO_SPLIT_INC_*_MASK
Defaults env_keep += CISCO_SPLIT_INC_*_MASKLEN
Defaults env_keep += CISCO_SPLIT_INC_*_PROTOCOL
Defaults env_keep += CISCO_SPLIT_INC_*_SPORT
Defaults env_keep += CISCO_SPLIT_INC_*_DPORT
Defaults env_keep += CISCO_IPV6_SPLIT_INC
Defaults env_keep += CISCO_IPV6_SPLIT_INC_*_ADDR
Defaults env_keep += CISCO_IPV6_SPLIT_INC_*_MASKLEN

openconnect ALL=(ALL) NOPASSWD: /etc/vpnc/vpnc-script
```
