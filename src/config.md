# Configure openconnect

Create `/etc/openconnect/nis.config`:

```
protocol=pulse
interface=oc0
script=sudo /etc/vpnc/vpnc-script
certificate=/etc/openconnect/waldrep.crt
sslkey=/etc/openconnect/waldrep.rsa
no-system-trust
cafile=/etc/ssl/certs/USERTrust_RSA_Certification_Authority.pem
```

Add the PEM encoded certificate and key as indicated in the config file.
Note that the key is cleartext, so make sure the permissions are set correctly.
Also, ensure this key is not useful for anything other than VPN auth.

The [documentation][nonroot] suggests the script command should be `sudo -E
...`, but this doesn't seem to work right.
`sudo` thrown an error about not being able to preserve the environment, even
though we set that as the default (see the [sudo][sudo] page).

```
[root@lithium openconnect]# ls -lha
total 20K
drwxr-xr-x  2 openconnect openconnect 4.0K May  7 12:57 .
drwxr-xr-x 42 root        root        4.0K May  6 20:30 ..
-rw-------  1 openconnect openconnect  220 May  7 12:57 nis.config
-rw-------  1 openconnect openconnect 1.2K May  6 17:58 waldrep.crt
-rw-------  1 openconnect openconnect 1.7K May  6 14:00 waldrep.rsa
```

[nonroot]: https://www.infradead.org/openconnect/nonroot.html
[sudo]: sudo.md
