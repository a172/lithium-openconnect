# tun interface

The root user has to create the tunnel interface for openconnect to use.
It can be made so that an unprivileged user has access to use it.

## Manually creating the interface

```bash
ip tuntap add oc0 mode tun user openconnect
```

## Let networkd create the interface

Create `/etc/systemd/network/75-openconnect.netdev`

```
[NetDev]
Description = Permanent tun device for privilege separation of openconnect
Name = oc0
Kind = tun

[Tun]
User = openconnect
```
